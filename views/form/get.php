<?php

    /**
     * Форма изменения пароля по токену
     * (для неавторизированных пользователей)
     */

    // Без обращения к index - просмотр запрещен
    if (!defined('SYSTEM'))
    {
        die('Не разрешен просмотр');
    }


    $parts = parse_url($model->url_validate);

    parse_str($parts['query'], $query);
?>

<form action="<?=$parts['path']?>"
      class="xform_form_get"
      id="<?=$model->form_id?>"
      js_before="<?=$model->js_before?>"
      method="get">
<?php
foreach ($query as $key => $value) 
{
    $key = htmlspecialchars($key);
    $value = htmlspecialchars($value);
    echo '<input type="hidden" name="'.$key.'" value="'.$value.'" />';
}
?>