<?php

    /**
     * Форма изменения пароля по токену
     * (для неавторизированных пользователей)
     */

    // Без обращения к index - просмотр запрещен
    if (!defined('SYSTEM'))
    {
        die('Не разрешен просмотр');
    }

    // Добавляем файл JS для обработки формы
    \xtetis\xengine\helpers\StatResHelper::addTemplateJs(__DIR__ . '/../../web/js/xform.js');


?>

<form url_validate="<?=$model->url_validate?>"
      class="xform_form"
      type="<?=$model->form_type?>"
      id="<?=$model->form_id?>"
      js_before="<?=$model->js_before?>"
      method="post">
