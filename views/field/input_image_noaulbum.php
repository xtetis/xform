<?php

    // Без обращения к index - просмотр запрещен
    if (!defined('SYSTEM'))
    {
        die('Не разрешен просмотр');
    }

    /*

ПРимер вызова

echo \xtetis\xform\Component::renderField(
    [
        'template'   => 'input_image_noaulbum',
        'attributes' => [
            'label'      => 'Обложка вечеринки',
            'name'       => 'image_cover',
            'max_images' => 1,
        ],
    ]
)

    

*/



    // Имя элемента
    // -----------------------------------------------
    $name     = isset($attributes['name']) ? strval($attributes['name']) : '';
    $name_arr = $name . '[]';
    // -----------------------------------------------

    $label           = isset($attributes['label']) ? strval($attributes['label']) : '';
    $input_label_top = strlen($label) ? '<label>' . $label . '</label>' : '';



    // Максимальное количество изображений
    // -----------------------------------------------
    if (isset($attributes['max_images']))
    {
        $max_images = intval($attributes['max_images']);
        if ($max_images < 1)
        {
            $max_images = 1;
        }
    }
    else
    {
        $max_images = 1;
    }
    //-----------------------------------------------
    $uid = uniqid();

    $params_json = [
        'max_images' => $max_images,
    ];


    // Используется ли функция для загрузки внешних изображений
    $external_img_data     = isset($attributes['external_img_data']) ? intval($attributes['external_img_data']) : 0;

    // Урл для получения данных о внешнем изображении
    $url_get_external_img_data = \xtetis\xform\Component::makeUrl([
        'path'=>[
            'data',
            'ajax_external_img_data'
        ]
    ]);

?>

<div class="">
    <div class=" mb-3">
        <?=$input_label_top?>
        <div class="input-group cust-file-button">
            <div class="custom-file">
                <label for="file_input_<?=$uid?>"
                       class="custom-file-label my-custom-file-label">
                    Выберите изображение
                    <input type="file"
                           style="display:none;"
                           accept="image/png, image/jpeg"
                           class="custom-file-input xform_input_image_noalbum"
                           uid="<?=$uid?>"
                           set_name="<?=$name_arr?>"
                           max_images="<?=$max_images?>"
                           <?php if ($max_images > 1): ?>
                           multiple
                           <?php endif;?>
                           <?php if ($external_img_data): ?>
                           url_get_external_img_data="<?=$url_get_external_img_data?>"
                           <?php endif;?>
                           id="file_input_<?=$uid?>">
                </label>


            </div>
        </div>
        <input type="hidden"
               id="base64_<?=$uid?>_value">
        <div class="error_form__<?=$name?> form_error_item"></div>
        <div id="container_preview_<?=$uid?>"
             class="card-columns pt-3">
        </div>
        <div class="container_template_<?=$uid?>"
             style="display:none;">
            <div class="card mb-3 input_image_noalbum_card_<?=$uid?>"
                 style="box-shadow: 10px 10px 5px 0px rgba(0,0,0,0.75);">
                <input type="hidden"
                       class="b64_img_val">
                <img class="img-fluid card-img-top"
                     src=""
                     alt="Card image cap">
                <div class="card-body"
                     style="    text-align: center;">
                    <button type="button"
                            class="btn_delete_img_input_image_noalbum btn btn-danger">
                        Удалить
                    </button>
                </div>
            </div>
        </div>
    </div>

</div>
