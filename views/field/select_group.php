<?php
    // Без обращения к index - просмотр запрещен
    if (!defined('SYSTEM'))
    {
        die('Не разрешен просмотр');
    }


    $name = isset($attributes['name'])?strval($attributes['name']):'';
    $input_name = strlen($name)?'name="'.$name.'"':'';

    $class = isset($attributes['class'])?strval($attributes['class']):'form-control';
    $input_class = strlen($class)?'class="'.$class.'"':'';

    $multiple = isset($attributes['multiple'])?'multiple':'';

    $value = isset($value)?$value:[];


    $label = isset($attributes['label'])?strval($attributes['label']):'';
    $input_label = strlen($label)?'<label for="">'.$label.'</label>':'';

    $prepend_feather = isset($attributes['prepend_feather'])?strval($attributes['prepend_feather']):'';
    $input_prepend_feather = strlen($prepend_feather)?'
    <div class="input-group-prepend">
        <span class="input-group-text"><i data-feather="'.$prepend_feather.'"></i></span>
    </div>
    ':'';

    $options = [];
    $options_str = '';

    if  (
        (isset($attributes['options'])) &&
        (is_array($attributes['options']))
    )
    {
        foreach ($attributes['options'] as $optgroup_name => $optgroup_options) 
        {
            $options_str.='<optgroup label="'.$optgroup_name.'">';
            foreach ($optgroup_options as $k => $v) 
            {
                $selected = '';
            
                if  ((is_array($value) && in_array($k,$value)) || (is_string($value) && ($value == $k)) || (is_integer($value) && ($value == $k)))
                {
                    $selected = ' selected="selected" ';
                }
                $options_str.='<option '.$selected.' value="'.$k.'">'.$v.'</option>';
            }
    
            $options_str.='</optgroup>';
        }
    }

?>



<div class=" mb-3">
    <?=$input_label?>
    <div class="input-group">
        <?=$input_prepend_feather?>
        <select <?=$input_class?> <?=$input_name?>  <?=$multiple?> >
            <?=$options_str?>
        </select>
    </div>
    <div class="error_form__<?=$name?> form_error_item"></div>
</div>