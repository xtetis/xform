<?php

    /**
     * Элемент ввода multiple select
     */

    // Без обращения к index - просмотр запрещен
    if (!defined('SYSTEM'))
    {
        die('Не разрешен просмотр');
    }


    $uid = uniqid();

    // Имя поля ввода
    $name = isset($attributes['name'])?strval($attributes['name']):'';
    $input_name = strlen($name)?'name="'.$name.'"':'';

    // Имя ввода элемента для получения выбранного уровня вложенности
    // (используется если сделать выбираемым не только последний уровень вложенности)
    $input_name_level = strlen($name)?'name="'.$name.'_level"':'';


    // placeholder для поля ввода
    $placeholder = isset($attributes['placeholder'])?$attributes['placeholder']:'Выберите из списка';
    
    // Текстовое значение в поле ввода
    $selected_text = isset($attributes['selected_text'])?$attributes['selected_text']:'';

    
    // Текущее значение
    $value = isset($value)?intval($value):0;


    $label = isset($attributes['label'])?strval($attributes['label']):'';
    $input_label = strlen($label)?'<label for="">'.$label.'</label>':'';

    // Урл получения данных для дерева jstree
    $data_url = isset($attributes['data_url'])?strval($attributes['data_url']):'';
?>



<div class=" mb-3"> 
    <?=$input_label?>
    <div class="input-group">
        <input type="text"
               class="form-control select_multilevel_selected_text_<?=$uid?> <?=$name?>_select_multilevel_selected_text"
               placeholder="<?=$placeholder?>"
               readonly="readonly"
               value="<?=$selected_text?>">
        <input type="hidden" <?=$input_name?> uid="<?=$uid?>" class="select_multilevel_hidden_value_<?=$uid?>" value="<?=$value?>">
        <div class="input-group-append">
            <button type="button"
                    class="btn  btn-primary"
                    data-toggle="modal"
                    data-target="#modal__select_multiple_tree_<?=$uid?>"><?=$placeholder?></button>
        </div>
    </div>
    <div class="error_form__<?=$name?> form_error_item"></div>
</div>




<div id="modal__select_multiple_tree_<?=$uid?>"
     class="modal fade"
     tabindex="-1"
     aria-labelledby="exampleModalLiveLabel"
     style="display: none;"
     aria-hidden="true">
    <div class="modal-dialog"
         role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title"
                    id="exampleModalLiveLabel"><?=$label?></h5>
                <button type="button"
                        class="close"
                        data-dismiss="modal"
                        aria-label="Close"><span aria-hidden="true">×</span></button>
            </div>
            <div class="modal-body">
                <div class="jstree_multiple_select" data_url="<?=$data_url?>"  uid="<?=$uid?>">
                </div>
            </div>
            <div class="modal-footer">
                <button type="button"
                        class="btn  btn-secondary"
                        data-dismiss="modal">Отмена</button>
            </div>
        </div>
    </div>
</div>
