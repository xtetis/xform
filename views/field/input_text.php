<?php
    // Без обращения к index - просмотр запрещен
    if (!defined('SYSTEM'))
    {
        die('Не разрешен просмотр');
    }

    $input_type = isset($attributes['type'])?'type="'.$attributes['type'].'"':'type="text"';

    $name = isset($attributes['name'])?strval($attributes['name']):'';
    $input_name = strlen($name)?'name="'.$name.'"':'';

    $readonly = isset($attributes['readonly'])?strval($attributes['readonly']):'';
    $input_readonly = strlen($readonly)?'readonly="'.$readonly.'"':'';


    $style = isset($attributes['style'])?strval($attributes['style']):'';
    $input_style = strlen($style)?'style="'.$style.'"':'';


    

    $class = isset($attributes['class'])?strval($attributes['class']):'form-control';
    $input_class = strlen($class)?'class="'.$class.'"':'';

    $placeholder = isset($attributes['placeholder'])?strval($attributes['placeholder']):'';
    $input_placeholder = strlen($placeholder)?'placeholder="'.$placeholder.'"':'';

    $value = isset($value)?strval($value):'';
    $input_value = strlen($value)?'value="'.htmlspecialchars($value, ENT_QUOTES).'"':'';

    $label = isset($attributes['label'])?strval($attributes['label']):'';
    $input_label = strlen($label)?'<label for="">'.$label.'</label>':'';

    $prepend_feather = isset($attributes['prepend_feather'])?strval($attributes['prepend_feather']):'';
    $input_prepend_feather = strlen($prepend_feather)?'
    <div class="input-group-prepend">
        <span class="input-group-text"><i data-feather="'.$prepend_feather.'"></i></span>
    </div>
    ':'';
?>

<div class=" mb-3">
    <?=$input_label?>
    <div class="input-group">
        <?=$input_prepend_feather?>
        <input <?=$input_type?>
               <?=$input_name?>
               <?=$input_readonly?>
               <?=$input_style?>
               <?=$input_class?>
               <?=$placeholder?>
               <?=$input_value?>
               >
    </div>
    <div class="error_form__<?=$name?> form_error_item"></div>
</div>