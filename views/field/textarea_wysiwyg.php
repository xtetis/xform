<?php
    // Без обращения к index - просмотр запрещен
    if (!defined('SYSTEM'))
    {
        die('Не разрешен просмотр');
    }

    // Добавляем файл JS для обработки формы
    \xtetis\xengine\helpers\StatResHelper::addTemplateJs(__DIR__ . '/../../web/js/nic_edit.js');


    $name = isset($attributes['name'])?strval($attributes['name']):'';
    $input_name = strlen($name)?'name="'.$name.'"':'';

    $class = isset($attributes['class'])?strval($attributes['class']):'form-control';
    $input_class = strlen($class)?'class="'.$class.'"':'';

    $placeholder = isset($attributes['placeholder'])?strval($attributes['placeholder']):'';
    $input_placeholder = strlen($placeholder)?'placeholder="'.$placeholder.'"':'';

    $value = isset($value)?strval($value):'';
    $input_value = strlen($value)?'value="'.$value.'"':'';

    $label = isset($attributes['label'])?strval($attributes['label']):'';
    $input_label = strlen($label)?'<label for="">'.$label.'</label>':'';
?>

<div class=" mb-3">
    <?=$input_label?>
    <div class="input-group">
        <textarea
               <?=$input_name?>
               <?=$input_class?>
               <?=$placeholder?>
               
               ><?=$input_value?></textarea>
    </div>
    <div class="error_form__<?=$name?> form_error_item"></div>
</div>