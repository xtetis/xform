<?php
    // Без обращения к index - просмотр запрещен
    if (!defined('SYSTEM'))
    {
        die('Не разрешен просмотр');
    }



    $name       = isset($attributes['name']) ? strval($attributes['name']) : '';
    $input_name = strlen($name) ? 'name="' . $name . '"' : '';

    $value = isset($value) ? intval($value) : 0;

    $label           = isset($attributes['label']) ? strval($attributes['label']) : '';
    $input_label     = '<label for="" class="custom-file-label">Выберите изображение</label>';
    $input_label_top = strlen($label) ? '<label>' . $label . '</label>' : '';

    $uid = uniqid();


    if (!$value)
    {
        echo '<div>Не указан gallery_id для поля input_image в value</div>';

        return;
    }

    
    $gallery_model = \xtetis\ximg\models\GalleryModel::generateModelById($value);

    // Урл удаления изображения из галереи
    $url_delete_image = '/';

    // Урл для алминки для загрузки изображения в галерею
    $url_upload_image_to_gallery =  '/';


    if (isset($attributes['url_delete_image']))
    {
        $url_delete_image = strval($attributes['url_delete_image']);

        if ($url_delete_image == 'cms')
        {
            $url_delete_image = \xtetis\xcms\Component::makeUrl([
                'path'=>[
                    'gallery',
                    'ajax_delete_image'
                ],
            ]);
        }
    }

    if (isset($attributes['url_upload_image_to_gallery']))
    {
        $url_upload_image_to_gallery = strval($attributes['url_upload_image_to_gallery']);

        if ($url_upload_image_to_gallery == 'cms')
        {
            $url_upload_image_to_gallery = \xtetis\xcms\Component::makeUrl([
                'path'=>[
                    'gallery',
                    'ajax_upload_image_to_gallery'
                ],
            ]);
        }
    }

    $show_images_list = false;
    if (isset($attributes['show_images_list']))
    {
        $show_images_list = intval($attributes['show_images_list']);
    }
?>

<div class="">
    <input type="hidden"
           id="gallery_id_<?=$uid?>"
           value="<?=$value?>"
           <?=$input_name?>>

    <div class=" mb-3">
        <?=$input_label_top?>
        <div class="input-group cust-file-button">
            <div class="custom-file">
                <input type="file"
                       accept="image/png, image/jpeg"
                       class="custom-file-input xform_input_file_gallery_img"
                       uid="<?=$uid?>">
                <?=$input_label?>
            </div>
            <div class="input-group-append">
                <button class="btn  btn-primary btn__upload_img_to_gallery"
                        uid="<?=$uid?>"
                        url_upload_image_to_gallery="<?=$url_upload_image_to_gallery?>"
                        type="button">Загрузить </button>
            </div>
        </div>
        <input type="hidden"
               id="base64_<?=$uid?>_value">
        <div class="error_form__<?=$uid?> form_error_item"></div>
        <div id="container_preview_<?=$uid?>"
             class="container_preview img_upload_preview_container">
            <img src=""
                 id="img_<?=$uid?>_preview"
                 alt="">
        </div>
    </div>

    <?php if ($show_images_list):?>
    <div class="p-3 gallery_container gallery_<?=$value?>_container"
         style="background: gray;">
        <div class="gallery_<?=$value?>_container_inner">
            <div class="card-columns">
                <?php foreach ($gallery_model->getImgModelList() as $id_img => $model_img): ?>
                <div class="card">
                    <img class="img-fluid card-img-top"
                         src="<?=$model_img->getImgSrc()?>"
                         alt="Card image cap">
                    <div class="card-body">
                        <a class="btn  btn-danger btn_cms_delete_image"
                           href="javascript:void(0)"
                           id_gallery="<?=$value?>"
                           url_delete_image="<?=$url_delete_image?>"
                           idx="<?=$model_img->id?>">Удалить</a>
                    </div>
                </div>
                <?php endforeach;?>
            </div>
        </div>
    </div>
    <?php endif; ?>
</div>
