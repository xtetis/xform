<?php
    // Без обращения к index - просмотр запрещен
    if (!defined('SYSTEM'))
    {
        die('Не разрешен просмотр');
    }


    $name = isset($attributes['name'])?strval($attributes['name']):'';
    $input_name = strlen($name)?'name="'.$name.'"':'';

    $class = isset($attributes['class'])?strval($attributes['class']):'form-control';
    $input_class = strlen($class)?'class="'.$class.'"':'';

    $placeholder = isset($attributes['placeholder'])?strval($attributes['placeholder']):'';
    $input_placeholder = strlen($placeholder)?'placeholder="'.$placeholder.'"':'';

    $value = isset($value)?strval($value):'';

    $label = isset($attributes['label'])?strval($attributes['label']):'';
    $input_label = strlen($label)?'<label for="">'.$label.'</label>':'';

    $readonly = isset($attributes['readonly'])?strval($attributes['readonly']):'';
    $input_readonly = strlen($readonly)?'readonly="'.$readonly.'"':'';

    $style = isset($attributes['style'])?strval($attributes['style']):'';
    $input_style = strlen($style)?'style="'.$style.'"':'';
?>

<div class=" mb-3">
    <?=$input_label?>
    <div class="input-group">
        <textarea
               <?=$input_name?>
               <?=$input_class?>
               <?=$input_readonly?>
               <?=$input_placeholder?>
               <?=$input_style?>
               ><?=$value?></textarea>
    </div>
    <div class="error_form__<?=$name?> form_error_item"></div>
</div>