<?php

namespace xtetis\xform\models;

// Запрет прямого обращения
if (!defined('SYSTEM'))
{
    die('Не разрешен просмотр');
}

class ExternalImgModel extends \xtetis\xengine\models\Model
{
    /**
     * Урл с изображением
     */
    public $url = '';

    /**
     * BASE64 файла
     */
    public $file_base64 = '';

    /**
     * BASE64 файла для возврата
     */
    public $file_base64_return = '';

    /**
     * Данные файла
     */
    public $file_data = false;

    /**
     * MIME тип файла
     */
    public $mime_content_type = '';

    /**
     * Допустимые MIME типы файла
     */
    public $allow_mime_type_list = [
        'image/jpeg'          => 'jpg',
        'image/x-citrix-jpeg' => 'jpg',
        'image/pjpeg'         => 'jpg',
        'image/png'           => 'png',
    ];


    /**
     * Максимальный размер файла в байтах 512kb
     */
    public $max_filesize = 2 * 1024 * 1024;

    /**
     * Размер файла
     */
    public $filesize = 0;

    /**
     * Обрабатывает данные
     */
    public function run()
    {
        if ($this->getErrors())
        {
            return false;
        }

        $this->url = strval($this->url);
        $this->file_data = @file_get_contents($this->url);

        if (!$this->file_data)
        {
            $this->addError('file_data', 'Не удалось получить файл');

            return false;
        }

        $this->filesize = strlen($this->file_data);

        // Проверяем размер файла
        if (!$this->filesize)
        {
            $this->addError('filename', 'Размер файла - пустой');

            return false;
        }

        if ($this->filesize > $this->max_filesize)
        {
            $this->addError('filesize', 'Максимальный размер обрабатываемого файла ' . $this->max_filesize . ' b');

            return false;
        }


        // Проверяем MIME тип
        $fh = fopen('php://memory', 'w+b');
        fwrite($fh, $this->file_data);
        $this->mime_content_type = mime_content_type($fh);
        fclose($fh);


        if (!$this->mime_content_type)
        {
            $this->addError('mime_content_type', 'Не удалось получить MIME тип файла');

            return false;
        }

        if (!isset($this->allow_mime_type_list[$this->mime_content_type]))
        {
            $this->addError('mime_content_type', 'Недопустимый MIME тип файла ' . $this->mime_content_type);

            return false;
        }

        $this->file_base64 = base64_encode($this->file_data);

        $this->file_base64_return = 'data:'.$this->mime_content_type.';base64,'.$this->file_base64;

        return true;
    }

}
