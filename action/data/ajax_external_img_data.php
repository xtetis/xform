<?php

/**
 * Валидация при добавлении поста
 */

// Без обращения к index - просмотр запрещен
if (!defined('SYSTEM'))
{
    die('Не разрешен просмотр');
}

$model = new \xtetis\xform\models\ExternalImgModel();

// Загружает данные из POST параметров
$model->loadPostData();

if ($model->run())
{
    $response['result']      = true;
    $response['result_str']  = 'OK';
    $response['file_base64'] = $model->file_base64_return;
}
else
{
    $response['result']     = false;
    $response['result_str'] = $model->getLastErrorMessage();
}

echo \xtetis\xengine\helpers\JsonHelper::arrayToJson($response);
exit;
