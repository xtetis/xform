<?php

namespace xtetis\xform\controllers;

// Без обращения к index - просмотр запрещен
if (!defined('SYSTEM'))
{
    die('Не разрешен просмотр');
}

class FormController extends \xtetis\xengine\models\Model
{

    /**
     * ID формы
     */
    public $form_id = '';

    /**
     * Тип формы
     */
    public $form_type = '';

    /**
     * Типs формы
     */
    public $form_type_list = [
        'ajax',
        'get',
    ];

    /**
     * Урл для валидации
     */
    public $url_validate = '';

    /**
     * ПОля формы в виде HTML
     */
    public $html_fields = '';

    /**
     * Скрипт JS при успешной валидации формы
     */
    public $js_success = '';

    /**
     * Скрипт JS при ошибках при валидации формы
     */
    public $js_error = '';

    /**
     * Скрипт JS перед отправкой ajax формы
     */
    public $js_before = '';

    /**
     * Возвращает директорию, в которой находятся Views
     */
    public static function getPackageViewsDir()
    {
        return __DIR__ . '/../views/';
    }

    /**
     * Рендерит форму (начало формы)
     */
    public function renderOnlyFormStart()
    {
        if ($this->getErrors())
        {
            return false;
        }

        $this->form_id      = strval($this->form_id);
        $this->form_type    = strval($this->form_type);
        $this->url_validate = strval($this->url_validate);
        $this->html_fields  = strval($this->html_fields);
        $this->js_success   = strval($this->js_success);
        $this->js_error     = strval($this->js_error);
        $this->js_before    = strval($this->js_before);

        if (!strlen($this->form_type))
        {
            $this->addError('form_type', 'Не указан form_type');

            return false;
        }

        if (!strlen($this->url_validate))
        {
            $this->addError('url_validate', 'Не указан url_validate');

            return false;
        }

        if (!in_array($this->form_type, $this->form_type_list))
        {
            $this->addError('form_type', 'Некорректный тип формы form_type');

            return false;

        }

        return \xtetis\xengine\helpers\RenderHelper::renderFile(
            self::getPackageViewsDir() . 'form/' . $this->form_type . '.php',
            [
                'model' =>$this
            ]
        );

    }
}
