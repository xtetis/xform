<?php

namespace xtetis\xform\controllers;

// Без обращения к index - просмотр запрещен
if (!defined('SYSTEM'))
{
    die('Не разрешен просмотр');
}

class FieldController extends \xtetis\xengine\models\Model
{
    /**
     * Шаблон поля
     */
    public $template = '';

    /**
     * Аттрибуты поля
     */
    public $attributes = [];

    /**
     * Значение поля
     */
    public $value = '';



    /**
     * Дополнительная директория для поиска полей
     */
    public $additional_field_search_folder = '';

    /**
     * @param array $params
     */
    public function __construct($params = [])
    {

        if ($this->getErrors())
        {
            return false;
        }

        $allow_create_params = [
            'template',
            'attributes',
            'value',
            'value_assign_key'
        ];

        foreach ($allow_create_params as $allow_create_params_item)
        {
            if (
                (isset($params[$allow_create_params_item])) &&
                (property_exists($this, $allow_create_params_item))
            )
            {
                $this->$allow_create_params_item = $params[$allow_create_params_item];
            }
        }

    }

    /**
     * Возвращает директорию, в которой находятся Views
     */
    public static function getPackageViewsDir()
    {
        return __DIR__ . '/../views/';
    }

    /**
     * Рендерит поле согласно переданным параметрам
     */
    public function renderField()
    {
        if ($this->getErrors())
        {
            return false;
        }

        if (!strlen($this->template))
        {
            $this->addError('template', 'Не указан template');

            return false;
        }

        if (!is_array($this->attributes))
        {
            $this->attributes = [];
        }

        // Дополнительная директория для поиска полей
        $this->additional_field_search_folder = \xtetis\xengine\App::getParam('xfield_field_search_folder', '');

        // Если найдена установленная ранее директория
        if (strlen($this->additional_field_search_folder))
        {
            if (file_exists($this->additional_field_search_folder . '/' . $this->template . '.php'))
            {
                return \xtetis\xengine\helpers\RenderHelper::renderFile(
                    $this->additional_field_search_folder . '/' . $this->template . '.php',
                    [
                        'value'      => $this->value,
                        'attributes' => $this->attributes,
                    ]
                );
            }
        }



        if (!file_exists(self::getPackageViewsDir() . 'field/' . $this->template . '.php'))
        {
            $this->addError('template', 'Некорректный тип поля template '.(\xtetis\xengine\Config::isTest()?$this->template:''));

            return false;
        }

        return \xtetis\xengine\helpers\RenderHelper::renderFile(
            self::getPackageViewsDir() . 'field/' . $this->template . '.php',
            [
                'value'      => $this->value,
                'attributes' => $this->attributes,
            ]
        );

    }
}
