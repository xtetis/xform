# xtetis\xform
## Модуль для xEngine для создания и обработки форм


Powered by xTetis

---
## Установка модуля

 - Если Вы используете xEngine (https://bitbucket.org/xtetis/xengine/), то он содержит в ноде require файла composer.json, потому будет установлен автоматически
```sh
"xtetis/xform": "dev-master",
```

Пример использования (рендер формы)

```php
<?=\xtetis\xform\Component::renderOnlyFormStart([
    'url_validate' => $url_validate_form,
    'form_type'    => 'ajax',
    'js_success'   => "xuser.goToUrl('".$url_pagemessage."');",
    'js_error'     => 'xcaptcha.reload();',
]);?>
    // Список полей ввода
<button type="submit"
        class="btn btn-block btn-primary mb-4">Отправить</button>
<?=\xtetis\xform\Component::renderFormEnd();?>
```
Генерация поля
```php
<?=\xtetis\xform\Component::renderField(['template'=>'xuser_input_email'])?>
```

Установк адополнительной директории для поиска полей
```php
// Добавляет папку для поиска полей для xform
\xtetis\xform\Component::addFieldSearchFolder(self::getPackageViewsDir().'field');
```
## Обратная связь

Для связи с автором автором
skype: xtetis
telegram: @xtetis
