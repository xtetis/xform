var xform_response;


/*
Данные получаемые при валидации
[
    'result' => true/false,
    'js_success'=> 'user.goToUrl();', // JS который будет выполнен, обычно имя функции
    'js_fail'=> 'xcaptcha.reload();', // JS который будет выполнен, обычно имя функции
    'data'=>[
        'url_success' => '/',  Данные для вызываемой функции
    ],
    'errors'=>[
        'field_name'=>'error_message',
        ...
    ]
]
*/

class xform {
    /**
     * Валидация формы
     * 
     * @param {*} form 
     */
    static validateForm(form) {
        var type = $(form).attr('type');
        if (type == 'ajax') {
            this.validateAjaxForm(form);
            return false;
        }
    }


    /**
     * Вызов пользовательской функции
     * 
     * @param {*} script 
     */
    static callScript(script) {
        try {
            eval(script);
        } catch (error) {
            console.log(error);
        }

    }





    /**
     * Выводим содержимое ноды xform_response.html в блок  .ajax_response
     */
    static outAjaxResponse() {
        $('.ajax_response').html(xform_response.html);
    }





    /**
     * Очищаем содержимое блока .ajax_response
     */
    static clearAjaxResponse() {
        $('.ajax_response').html('');
    }


    /**
     * Обработка ошибко
     * 
     * @param {*} obj 
     */
    static prepareErrors(form, obj) {
        $.each(obj.errors, function(i, item) {
            if (i == 'common') {
                showMessage(item);
                return true;
            } else {
                if (!$(form).find('.error_form__' + i).length) {
                    showMessage(item);
                    return true;
                } else {
                    $(form).find('.error_form__' + i)
                        .html(item)
                        .show();
                }
            }
        });
    }

    /**
     * Обработка результат валидации
     */
    static getValidateResponse() {
        return xform_response;
    }

    /**
     * Обработка AJAX формы
     * 
     * @param {*} form 
     */
    static validateAjaxForm(form) {
        var formdata = $(form)
            .serialize();

        $(form).find('.form_error_item')
            .html('')
            .hide();

        var url_validate = $(form).attr('url_validate');

        // Чтоб нужно выполнить перед отправкой формы
        var js_before = $(form).attr('js_before');
        js_before = js_before || '';
        if (js_before.length > 0) {
            xform.callScript(js_before);
        }


        $.post(url_validate, formdata, function(obj) {
                xform_response = obj;
                console.log(obj);

                if (obj.result) {
                    if ("js_success" in obj) {
                        obj.js_success = obj.js_success || '';
                        if (obj.js_success.length > 0) {
                            xform.callScript(obj.js_success);
                        }
                    }
                } else {
                    xform.prepareErrors(form, obj);
                    if ("js_fail" in obj) {
                        obj.js_fail = obj.js_fail || '';
                        if (obj.js_fail.length > 0) {
                            xform.callScript(obj.js_fail);
                        }
                    }
                }
            },
            'json');
    }

    static goToUrl() {
        var obj = xform.getValidateResponse();
        console.log(obj)
        window.location.href = obj.data.go_to_url;
    }


    /**
     * Используется для предварительного показа изображения перед 
     * загрузкой в альбом из элемента imput:file
     * 
     * @param {*} input 
     */
    static readFileInputToImage(input) {
        var uid = $(input).attr('uid');
        var id_preview = '#img_' + uid + '_preview';
        var i_base64 = '#base64_' + uid + '_value';
        console.log(input.files);
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function(e) {
                console.log('e');
                console.log(e);
                $('#container_preview_' + uid).show();
                $(id_preview).attr('src', e.target.result);
                $(id_preview).hide();
                $(id_preview).fadeIn(650);
                $(id_preview).css('display', 'block');
                $(i_base64).val(e.target.result);
            }
            console.log(input.files[0]);
            reader.readAsDataURL(input.files[0]);
        }
    }


    /**
     * Используется для предварительного показа изображения/изображений 
     * перед загрузкой на сервер
     * 
     * @param {*} input 
     */
    static readFileInputImageNoalbum(input) {

        var uid = $(input).attr('uid');
        var set_name = $(input).attr('set_name');
        var max_images = Number($(input).attr('max_images'));


        var names = [];
        for (var i = 0; i < $(input).get(0).files.length; ++i) {
            var reader = new FileReader();
            reader.onload = function(e) {

                var current_count_images = $('.input_image_noalbum_card_' + uid).length;
                current_count_images = Number(current_count_images);
                if (current_count_images > max_images) {
                    showMessage('Максимальный количество изображений для этого поля - ' + max_images);
                    return;
                }

                console.log(e);
                // Взяли шаблон
                var template = $('.container_template_' + uid).html();

                template = $(template).find('img').attr('src', e.target.result).end()[0].outerHTML;
                template = $(template).find('.b64_img_val').attr('name', set_name).end()[0].outerHTML;
                template = $(template).find('.b64_img_val').val(e.target.result).end()[0].outerHTML;

                $('#container_preview_' + uid).append(template);
            }


            if ($(input).get(0).files[i].type.match('image.*')) {
                console.log($(input).get(0).files[i]);
                if ($(input).get(0).files[i].size > (500 * 1024)) {
                    showMessage('Максимальный размер изображений 500 kB');
                } else {
                    reader.readAsDataURL($(input).get(0).files[i]);
                }

            } else {
                showMessage('Допустима загрузка только изображений');
            }


        }


    }


    /**
     * Используется для предварительного показа изображения/изображений 
     * перед загрузкой на сервер по урлу
     * 
     * @param {*} input 
     */
    static readUrlToImageNoalbum(input, url) {

        var uid = $(input).attr('uid');
        var set_name = $(input).attr('set_name');
        var max_images = Number($(input).attr('max_images'));

        var url_get_external_img_data = $(input).attr('url_get_external_img_data');;

        $.post(url_get_external_img_data, {
            url: url
        }, function(obj) {

            xform_response = obj;
            console.log(obj);
            $('#container_preview_' + uid).html('');
            if (obj.result)
            {
                var current_count_images = $('.input_image_noalbum_card_' + uid).length;
                current_count_images = Number(current_count_images);
                if (current_count_images > max_images) {
                    showMessage('Максимальный количество изображений для этого поля - ' + max_images);
                    return;
                }
    
                // Взяли шаблон
                var template = $('.container_template_' + uid).html();
    
                template = $(template).find('img').attr('src', obj.file_base64).end()[0].outerHTML;
                template = $(template).find('.b64_img_val').attr('name', set_name).end()[0].outerHTML;
                template = $(template).find('.b64_img_val').val(obj.file_base64).end()[0].outerHTML;
    
                
                $('#container_preview_' + uid).append(template);
            }
            else
            {
                showMessage(obj.result_str);
            }
        },
        'json');
    }



    /**
     * Загрузка изображения в галлерею
     */
    static uploadImgToGallery(input) {
        var uid = $(input).attr('uid');
        var img_value_b64 = $('#base64_' + uid + '_value').val();
        var gallery_id = $('#gallery_id_' + uid).val();
        var url_upload_image_to_gallery = $(input).attr('url_upload_image_to_gallery');


        $.post(url_upload_image_to_gallery, {
                id_gallery: gallery_id,
                type: 'base64',
                filedata: img_value_b64
            }, function(obj) {

                xform_response = obj;
                console.log(obj);

                console.log('obj.result = ' + obj.result);
                if (obj.result) {
                    if ("js_success" in obj) {
                        obj.js_success = obj.js_success || '';
                        if (obj.js_success.length > 0) {
                            xform.callScript(obj.js_success);
                        }

                    }
                    $(".gallery_" + gallery_id + "_container").load(window.location.href + " .gallery_" + gallery_id + "_container_inner");
                    $('#container_preview_' + uid).hide();
                } else {
                    xform.prepareErrors(obj, obj);
                    if ("js_fail" in obj) {
                        obj.js_fail = obj.js_fail || '';
                        if (obj.js_fail.length > 0) {
                            xform.callScript(obj.js_fail);
                        }
                    }
                }

            },
            'json');
    }
}


$(function() {

    $(document).on('submit', 'form.xform_form', function() {
        return xform.validateForm(this);
    });


    $(document.body).on('change', 'input.xform_input_file_gallery_img', function() {
        xform.readFileInputToImage(this);
    });

    // input_image_noalbum
    $(document.body).on('change', 'input.xform_input_image_noalbum', function() {
        xform.readFileInputImageNoalbum(this);
    });

    $(document.body).on('click', '.btn_delete_img_input_image_noalbum', function() {
        $(this).parent().parent().remove();

    });


    $(document.body).on('click', '.btn__upload_img_to_gallery', function() {
        xform.uploadImgToGallery(this);
    });


    let dp = new AirDatepicker('.date-control');

    //dp.show();


    // Дерево выбора
    $(".jstree_multiple_select").each(function(index) {
        $(this).on('changed.jstree', function(e, data) {
            var i, j = [];

            var uid = $(this).attr('uid');

            for (i = 0, j = data.selected.length; i < j; i++) {
                var id = data.instance.get_node(data.selected[i]).id;
                var idx = $('#' + id).attr('idx');
                var level = $('#' + id).attr('level');
                var name = $('#' + id).attr('name');
                var is_selectable = $('#' + id).attr('is_selectable');
                is_selectable = Number(is_selectable);

                if (is_selectable) {
                    $('.select_multilevel_selected_text_' + uid).val(name); 
                    $('#modal__select_multiple_tree_' + uid).modal('hide');
                    $('.select_multilevel_hidden_value_' + uid).val(idx);
                    $('.select_multilevel_hidden_level_' + uid).val(level);
                }
            }

        }).jstree({
            "core": {
                "multiple": false,
                "themes": {
                    "responsive": false
                },
                // so that create works
                "check_callback": true,
                'data': {
                    'url': $(this).attr('data_url'),
                    'data': function(node) {
                        return {
                            'parent': node.id
                        };
                    }
                }
            }
        });
    });

})