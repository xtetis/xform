<?php

namespace xtetis\xform;

class Component extends \xtetis\xengine\models\Component
{

    /**
     * Список действий модуля
     *
     * @var array
     */
    public $module_actions = [

    ];

    /**
     * Имя модуля в роутах (прописан в composer.json как autoload/psr-4 )
     *
     * @var string
     */
    public $module_name_in_routes = 'xtetis\xform';

    /**
     * Рендер формы
     */
    public static function renderOnlyFormStart($params = [])
    {
        $controllerForm = new \xtetis\xform\controllers\FormController($params);
        $form           = $controllerForm->renderOnlyFormStart();
        if ($controllerForm->getErrors())
        {
            throw new \Exception('Ошибка при создании формы xtetis\xform: ' . $controllerForm->getLastErrorMessage());
        }

        return $form;
    }

    /**
     * Рендер конца формы </form>
     */
    public static function renderFormEnd()
    {
        return '</form>';
    }

    /**
     * Рендер поля
     * 
     * Пример:
     * <?=\xtetis\xform\Component::renderField(
     *      [
     *          'template'   => 'input_text',
     *          'value'      => 'Значение поля',
     *          'attributes' => [
     *              ...
     *          ] 
     *      ]
     * )?>
     */
    public static function renderField($params = []): mixed
    {
        $controllerForm = new \xtetis\xform\controllers\FieldController($params);
        $form           = $controllerForm->renderField();
        if ($controllerForm->getErrors())
        {
            \xtetis\xengine\helpers\LogHelper::customDie('Ошибка при рендеринге поля xtetis\xform: ' . $controllerForm->getLastErrorMessage());
        }

        return $form;
    }

    /**
     * Добавляет папку для поиска полей для xform
     * 
     * @param $filder
     * @return mixed
     */
    public static function addFieldSearchFolder($folder = '')
    {
        if (!is_string($folder))
        {
            \xtetis\xengine\helpers\LogHelper::customDie('Ошибка при добавлении директории для поиска полей в xtetis\xform: не является строкой');
        }

        if (!file_exists($folder))
        {
            \xtetis\xengine\helpers\LogHelper::customDie('Ошибка при добавлении директории для поиска полей в xtetis\xform: директория не существует');
        }

        if (!is_dir($folder))
        {
            \xtetis\xengine\helpers\LogHelper::customDie('Ошибка при добавлении директории для поиска полей в xtetis\xform: не является директорией');
        }

        \xtetis\xengine\App::setParam('xfield_field_search_folder',$folder);
    }
}
